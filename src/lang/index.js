export default {
  pt: {
    about: {
      subhead: 'Sobre o Citrus'
    },
		lemonade: {
      hero: [
        'A Experiência',
        'do Cliente como',
        'você nunca viu.'
      ],
			about: 'Sobre o'
		},
		seed: {
			transform: {
				title: [
					'Transformando',
					'Limões em',
					'Limonada'
				]
			},
		},
		contact: {
			name: 'Nome',
			email: 'E-mail',
			company: 'Empresa',
			phone: 'Telefone',
			subject: 'Assunto',
			message: 'Mensagem',
			submit: 'Enviar',
			sending: 'Enviando'
		},
    footer: {
      group: `
Uma empresa do grupo <a href="https://digivox.com.br/" target="_blank">Digivox</a>
      `
    }
  },

  en: {
    about: {
      subhead: 'About Citrus'
    },
		lemonade: {
      hero: [
        'Customer experience',
        `as like you've never`,
        'seen before.'
      ],
			about: 'About the'
		},
		seed: {
			transform: {
				title: [
					'Transforming',
					'Lemons into',
					'Lemonade'
				]
			},
		},
		contact: {
			name: 'Name',
			email: 'E-mail',
			company: 'Company',
			phone: 'Phone',
			subject: 'Subject',
			message: 'Message',
			submit: 'Submit',
			sending: 'Sending'
		},
		footer: {
			group: `
A <a href="https://digivox.com.br/" target="_blank">Digivox</a> Business
      `
		}
  }
}
