import Vue from 'vue'
import Router from 'vue-router'
import CILanding from '@/components/pages/CILanding'
import CISuccess from '@/components/pages/CISuccess'
import CISeedProgram from '@/components/pages/CISeedProgram'
import CILemonade from '@/components/pages/CILemonade'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
		{
			path: '/',
			name: 'home',
			component: CILanding
		},
		{
			path: '/contato-sucesso',
			name: 'contato-sucesso',
			component: CISuccess
		},
		{
			path: '/contato-seed-sucesso',
			name: 'contato-seed-sucesso',
			component: CISuccess
		},
		{
			path: '/contato-lemonade-sucesso',
			name: 'contato-lemonade-sucesso',
			component: CISuccess
		},
		{
			path: '/seed',
			name: 'seed',
			component: CISeedProgram
		},
		{
			path: '/lemonade',
			name: 'lemonade',
			component: CILemonade
		}
	],
	scrollBehavior(to, from, savedPosition) {
		const container = document.querySelector('.ci-app__body')

		container.scrollTo({
			top: 0,
			behavior: 'smooth'
		})
	}
})

export default router
