import { storiesOf } from '@storybook/vue'
import CILanding from './index'

const style = {}

storiesOf('Template - ci-landing', module)
	.add('default', () => ({
		components: { 'ci-landing': CILanding },
		data: () => ({ style}),
		template: `
			<ci-landing />
		`
	}))
