import { storiesOf } from '@storybook/vue'
import CISeedProgram from './index'

const style = {}

storiesOf('Template - ci-seed-program', module)
	.add('default', () => ({
		components: { 'ci-seed-program': CISeedProgram },
		data: () => ({ style }),
		template: `
			<ci-seed-program />
		`
	}))
