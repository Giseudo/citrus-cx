import { storiesOf } from '@storybook/vue'
import CILemonade from './index'

storiesOf('Template - ci-lemonade', module)
	.add('default', () => ({
		components: { 'ci-lemonade': CILemonade },
		template: `
			<ci-lemonade />
		`
	}))
