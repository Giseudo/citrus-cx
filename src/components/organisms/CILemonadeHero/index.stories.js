import { storiesOf } from '@storybook/vue'
import CILemonadeHero from './index'

storiesOf('Organism - ci-lemonade-hero', module)
	.add('primary', () => ({
		components: { 'ci-lemonade-hero': CILemonadeHero },
		template: `
			<ci-lemonade-hero>
			</ci-lemonade-hero>
		`
	}))
