import { storiesOf } from '@storybook/vue'
import CILemonadeTalk from './index'

storiesOf('Organism - ci-lemonade-talk', module)
	.add('primary', () => ({
		components: { 'ci-lemonade-talk': CILemonadeTalk },
		template: `
			<ci-lemonade-talk>
			</ci-lemonade-talk>
		`
	}))
