import { storiesOf } from '@storybook/vue'
import CISeedTransform from './index'

import VueI18n from 'vue-i18n'
import messages from '@/lang'

storiesOf('Organism - ci-seed-transform', module)
	.add('default - pt', () => ({
		i18n: new VueI18n({ locale: 'pt', messages }),
		components: { 'ci-seed-transform': CISeedTransform },
		template: `
			<ci-seed-transform />
		`
	}))

	.add('default - en', () => ({
		i18n: new VueI18n({ locale: 'en', messages }),
		components: { 'ci-seed-transform': CISeedTransform },
		template: `
			<ci-seed-transform />
		`
	}))
