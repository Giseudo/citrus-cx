import { storiesOf } from '@storybook/vue'
import CILemonadeTour from './index'

storiesOf('Organism - ci-lemonade-tour', module)
	.add('primary', () => ({
		components: { 'ci-lemonade-tour': CILemonadeTour },
		template: `
			<ci-lemonade-tour>
			</ci-lemonade-tour>
		`
	}))
