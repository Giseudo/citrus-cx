import { storiesOf } from '@storybook/vue'
import CISuite from './index'

const style = {
}

storiesOf('Organism - ci-suite', module)
	.add('primary', () => ({
		components: { 'ci-suite': CISuite },
		data: () => ({ style}),
		template: `
			<ci-suite />
		`
	}))

