import { storiesOf } from '@storybook/vue'
import CIFooter from './index'

import VueI18n from 'vue-i18n'
import messages from '@/lang'

storiesOf('Organism - ci-footer', module)
	.add('default - pt', () => ({
		i18n: new VueI18n({ locale: 'pt', messages }),
		components: { 'ci-footer': CIFooter },
		template: `
			<ci-footer />
		`
	}))

	.add('default - en', () => ({
		i18n: new VueI18n({ locale: 'en', messages }),
		components: { 'ci-footer': CIFooter },
		template: `
			<ci-footer />
		`
	}))
