import { storiesOf } from '@storybook/vue'
import CISquad from './index'

const style = {}

storiesOf('Organism - ci-squad', module)
	.add('default', () => ({
		components: { 'ci-squad': CISquad },
		data: () => ({ style}),
		template: `
			<ci-squad />
		`
	}))
