import { storiesOf } from '@storybook/vue'
import CISeedPlanting from './index'

storiesOf('Organism - ci-seed-planting', module)
	.add('primary', () => ({
		components: { 'ci-seed-planting': CISeedPlanting },
		template: `
			<ci-seed-planting />
		`
	}))
