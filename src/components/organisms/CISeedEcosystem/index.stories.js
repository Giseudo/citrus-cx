import { storiesOf } from '@storybook/vue'
import CISeedEcosystem from './index'

storiesOf('Organism - ci-seed-ecosystem', module)
	.add('primary', () => ({
		components: { 'ci-seed-ecosystem': CISeedEcosystem },
		template: `
			<ci-seed-ecosystem />
		`
	}))
