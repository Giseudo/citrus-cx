import { storiesOf } from '@storybook/vue'
import CILemonadeDay from './index'

import VueI18n from 'vue-i18n'
import messages from '@/lang'

storiesOf('Organism - ci-lemonade-day', module)
	.add('default - pt', () => ({
		i18n: new VueI18n({ locale: 'pt', messages }),
		components: { 'ci-lemonade-day': CILemonadeDay },
		template: `
			<ci-lemonade-day>
			</ci-lemonade-day>
		`
	}))

	.add('default - en', () => ({
		i18n: new VueI18n({ locale: 'en', messages }),
		components: { 'ci-lemonade-day': CILemonadeDay },
		template: `
			<ci-lemonade-day>
			</ci-lemonade-day>
		`
	}))
