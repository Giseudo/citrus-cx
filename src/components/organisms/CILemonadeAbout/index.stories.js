import { storiesOf } from '@storybook/vue'
import CILemonadeAbout from './index'

storiesOf('Organism - ci-lemonade-about', module)
	.add('primary', () => ({
		components: { 'ci-lemonade-about': CILemonadeAbout },
		template: `
			<ci-lemonade-about>
			</ci-lemonade-about>
		`
	}))
