import { storiesOf } from '@storybook/vue'
import CILemonadeJourney from './index'

storiesOf('Organism - ci-lemonade-journey', module)
	.add('primary', () => ({
		components: { 'ci-lemonade-journey': CILemonadeJourney },
		template: `
			<ci-lemonade-journey>
			</ci-lemonade-journey>
		`
	}))
