import { storiesOf } from '@storybook/vue'
import CITryOut from './index'

const style = {}

storiesOf('Organism - ci-try-out', module)
	.add('default', () => ({
		components: { 'ci-try-out': CITryOut },
		data: () => ({ style }),
		template: `
			<ci-try-out :style="style" />
		`
	}))

