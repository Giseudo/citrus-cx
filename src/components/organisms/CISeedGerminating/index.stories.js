import { storiesOf } from '@storybook/vue'
import CISeedGerminating from './index'

storiesOf('Organism - ci-seed-germinating', module)
	.add('primary', () => ({
		components: { 'ci-seed-germinating': CISeedGerminating },
		template: `
			<ci-seed-germinating />
		`
	}))
