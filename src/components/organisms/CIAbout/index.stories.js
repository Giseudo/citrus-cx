import { storiesOf } from '@storybook/vue'
import CIAbout from './index'

import VueI18n from 'vue-i18n'
import messages from '@/lang'

const style = {
	hero: {
		height: '600px'
	}
}

storiesOf('Organism - ci-about', module)
	.add('default - pt', () => ({
		i18n: new VueI18n({ locale: 'pt', messages }),
		components: { 'ci-about': CIAbout },
		data: () => ({ style}),
		template: `
			<ci-about :style="style.hero">
			</ci-about>
		`
	}))

	.add('default - en', () => ({
		i18n: new VueI18n({ locale: 'en', messages }),
		components: { 'ci-about': CIAbout },
		data: () => ({ style}),
		template: `
			<ci-about :style="style.hero">
			</ci-about>
		`
	}))
