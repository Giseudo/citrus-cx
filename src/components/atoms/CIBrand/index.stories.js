import { storiesOf } from '@storybook/vue'
import CIBrand from './index'

const style = {
	container: {
	}
}

storiesOf('Molecule - ci-brand', module)
	.add('citrus', () => ({
		components: { 'ci-brand': CIBrand },
		data: () => ({ style: style }),
		template: `
			<ci-brand type="citrus" />
		`
	}))
	.add('suite', () => ({
		components: { 'ci-brand': CIBrand },
		data: () => ({ style: style }),
		template: `
			<ci-brand type="suite" />
		`
	}))

