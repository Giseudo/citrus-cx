import { storiesOf } from '@storybook/vue'
import CIText from './index'

const style = {
	container: {
		padding: '24px',
		background: 'white',
	},
	darkContainer: {
		padding: '24px',
		background: 'black'
	}
}

storiesOf('Atom - ci-text', module)
	.add('default', () => ({
		components: { 'ci-text': CIText },
		data: () => ({ style: style }),
		template: `
			<div>
				<div :style="style.container">
					<ci-text type="jumbo">
						Jumbo
					</ci-text>
					<ci-text type="title">
						Title
					</ci-text>
					<ci-text type="subhead">
						Subhead
					</ci-text>
					<ci-text type="body">
						Body
					</ci-text>
					<ci-text type="caption">
						Caption
					</ci-text>
				</div>

				<div :style="style.darkContainer">
					<ci-text type="jumbo" :dark="true">
						Jumbo
					</ci-text>
					<ci-text type="title" :dark="true">
						Title
					</ci-text>
					<ci-text type="subhead" :dark="true">
						Subhead
					</ci-text>
					<ci-text type="body" :dark="true">
						Body
					</ci-text>
					<ci-text type="caption" :dark="true">
						Caption
					</ci-text>
				</div>
			</div>
		`
	}))

	.add('primary', () => ({
		components: { 'ci-text': CIText },
		data: () => ({ style: style }),
		template: `
			<div>
				<div :style="style.container">
					<ci-text type="jumbo" theme="primary">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="primary">
						Title
					</ci-text>
					<ci-text type="subhead" theme="primary">
						Subhead
					</ci-text>
					<ci-text type="body" theme="primary">
						Body
					</ci-text>
					<ci-text type="caption" theme="primary">
						Caption
					</ci-text>
				</div>

				<div :style="style.darkContainer">
					<ci-text type="jumbo" :dark="true" theme="primary">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="primary">
						Title
					</ci-text>
					<ci-text type="subhead" theme="primary">
						Subhead
					</ci-text>
					<ci-text type="body" theme="primary">
						Body
					</ci-text>
					<ci-text type="caption" theme="primary">
						Caption
					</ci-text>
				</div>
			</div>
		`
	}))

	.add('accent', () => ({
		components: { 'ci-text': CIText },
		data: () => ({ style: style }),
		template: `
			<div>
				<div :style="style.container">
					<ci-text type="jumbo" theme="accent">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="accent">
						Title
					</ci-text>
					<ci-text type="subhead" theme="accent">
						Subhead
					</ci-text>
					<ci-text type="body" theme="accent">
						Body
					</ci-text>
					<ci-text type="caption" theme="accent">
						Caption
					</ci-text>
				</div>

				<div :style="style.darkContainer">
					<ci-text type="jumbo" :dark="true" theme="accent">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="accent">
						Title
					</ci-text>
					<ci-text type="subhead" theme="accent">
						Subhead
					</ci-text>
					<ci-text type="body" theme="accent">
						Body
					</ci-text>
					<ci-text type="caption" theme="accent">
						Caption
					</ci-text>
				</div>
			</div>
		`
	}))

	.add('warn', () => ({
		components: { 'ci-text': CIText },
		data: () => ({ style: style }),
		template: `
			<div>
				<div :style="style.container">
					<ci-text type="jumbo" theme="warn">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="warn">
						Title
					</ci-text>
					<ci-text type="subhead" theme="warn">
						Subhead
					</ci-text>
					<ci-text type="body" theme="warn">
						Body
					</ci-text>
					<ci-text type="caption" theme="warn">
						Caption
					</ci-text>
				</div>

				<div :style="style.darkContainer">
					<ci-text type="jumbo" :dark="true" theme="warn">
						Jumbo
					</ci-text>
					<ci-text type="title" theme="warn">
						Title
					</ci-text>
					<ci-text type="subhead" theme="warn">
						Subhead
					</ci-text>
					<ci-text type="body" theme="warn">
						Body
					</ci-text>
					<ci-text type="caption" theme="warn">
						Caption
					</ci-text>
				</div>
			</div>
		`
	}))

