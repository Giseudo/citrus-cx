import { storiesOf } from '@storybook/vue'
import CICircle from './index'

const style = {}

storiesOf('Atom - ci-circle', module)
	.add('primary', () => ({
		components: { 'ci-leaf': CICircle },
		data: () => ({ style}),
		template: `
			<ci-circle theme="primary" />
		`
	}))

storiesOf('Atom - ci-circle', module)
	.add('accent', () => ({
		components: { 'ci-circle': CICircle },
		data: () => ({ style}),
		template: `
			<ci-circle theme="accent" />
		`
	}))

storiesOf('Atom - ci-circle', module)
	.add('warn', () => ({
		components: { 'ci-circle': CICircle },
		data: () => ({ style}),
		template: `
			<ci-circle theme="warn" />
		`
	}))


