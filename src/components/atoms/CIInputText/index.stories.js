import { storiesOf } from '@storybook/vue'
import CIInputText from './index'

const style = {}

storiesOf('Atom - ci-input-text', module)
	.add('default', () => ({
		components: { 'ci-input-text': CIInputText },
		data: () => ({ style}),
		template: `
			<ci-input-text />
		`
	}))

	.add('textarea', () => ({
		components: { 'ci-input-text': CIInputText },
		data: () => ({ style}),
		template: `
			<ci-input-text type="textarea" />
		`
	}))
