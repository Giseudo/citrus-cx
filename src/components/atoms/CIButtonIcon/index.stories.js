import { storiesOf } from '@storybook/vue'
import CIButtonIcon from './index'

const style = {
	container: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-around'
	}
}

storiesOf('Molecule - ci-button-icon', module)
	.add('default', () => ({
		components: { 'ci-button-icon': CIButtonIcon },
		data: () => ({ style: style }),
		template: `
			<div :style="style.container">
				<ci-button-icon theme="default" name="twitter" size="sm" />
				<ci-button-icon theme="default" name="twitter" size="md" />
				<ci-button-icon theme="default" name="twitter" size="lg" />
			</div>
		`
	}))
	.add('primary', () => ({
		components: { 'ci-button-icon': CIButtonIcon },
		data: () => ({ style: style }),
		template: `
			<div :style="style.container">
				<ci-button-icon theme="primary" name="twitter" size="sm" />
				<ci-button-icon theme="primary" name="twitter" size="md" />
				<ci-button-icon theme="primary" name="twitter" size="lg" />
			</div>
		`
	}))
	.add('accent', () => ({
		components: { 'ci-button-icon': CIButtonIcon },
		data: () => ({ style: style }),
		template: `
			<div :style="style.container">
				<ci-button-icon theme="accent" name="twitter" size="sm" />
				<ci-button-icon theme="accent" name="twitter" size="md" />
				<ci-button-icon theme="accent" name="twitter" size="lg" />
			</div>
		`
	}))
	.add('warn', () => ({
		components: { 'ci-button-icon': CIButtonIcon },
		data: () => ({ style: style }),
		template: `
			<div :style="style.container">
				<ci-button-icon theme="warn" name="twitter" size="sm" />
				<ci-button-icon theme="warn" name="twitter" size="md" />
				<ci-button-icon theme="warn" name="twitter" size="lg" />
			</div>
		`
	}))

