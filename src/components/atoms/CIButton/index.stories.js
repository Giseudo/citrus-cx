import { storiesOf } from '@storybook/vue'
import CIButton from './index'

const style = {}

storiesOf('Atom - ci-button', module)
	.add('primary', () => ({
		components: { 'ci-button': CIButton },
		data: () => ({ style }),
		template: `
			<ci-button theme="primary">
				Lorem ipsum dolor 
			</ci-button>
		`
	}))
	.add('accent', () => ({
		components: { 'ci-button': CIButton },
		data: () => ({ style }),
		template: `
			<ci-button theme="accent">
				Lorem ipsum dolor 
			</ci-button>
		`
	}))
	.add('warn', () => ({
		components: { 'ci-button': CIButton },
		data: () => ({ style }),
		template: `
			<ci-button theme="warn">
				Lorem ipsum dolor 
			</ci-button>
		`
	}))
