import { storiesOf } from '@storybook/vue'
import CILeaf from './index'

const style = {}

storiesOf('Atom - ci-leaf', module)
	.add('primary', () => ({
		components: { 'ci-leaf': CILeaf },
		data: () => ({ style}),
		template: `
			<ci-leaf theme="primary" />
		`
	}))

storiesOf('Atom - ci-leaf', module)
	.add('accent', () => ({
		components: { 'ci-leaf': CILeaf },
		data: () => ({ style}),
		template: `
			<ci-leaf theme="accent" />
		`
	}))

storiesOf('Atom - ci-leaf', module)
	.add('warn', () => ({
		components: { 'ci-leaf': CILeaf },
		data: () => ({ style}),
		template: `
			<ci-leaf theme="warn" />
		`
	}))


