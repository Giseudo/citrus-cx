import { storiesOf } from '@storybook/vue'
import CIChannel from './index'

const style = {
	margin: '16px',
	width: '200px'
}

storiesOf('Molecule - ci-channel', module)
	.add('default', () => ({
		components: { 'ci-channel': CIChannel },
		data: () => ({
			style
		}),
		template: `
			<ci-channel :style="style" />
		`
	}))

