import { storiesOf } from '@storybook/vue'
import CIPlayer from './index'

const style = {}

storiesOf('Molecule - ci-player', module)
	.add('primary', () => ({
		components: { 'ci-player': CIPlayer },
		data: () => ({ style: style }),
		template: `
			<ci-player theme="primary">
			</ci-player>
		`
	}))

