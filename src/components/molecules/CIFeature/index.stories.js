import { storiesOf } from '@storybook/vue'
import CIFeature from './index'

const style = {
	background: 'black',
	padding: '24px'
}

storiesOf('Molecule - ci-feature', module)
	.add('default', () => ({
		components: { 'ci-feature': CIFeature },
		data: () => ({ style: style }),
		template: `
			<ci-feature :style="style" />
		`
	}))

