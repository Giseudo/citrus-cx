import { storiesOf } from '@storybook/vue'
import CISeedSystem from './index'

storiesOf('Molecule - ci-seed-system', module)
	.add('default', () => ({
		components: { 'ci-seed-system': CISeedSystem },
		template: `
			<ci-seed-system />
		`
	}))
