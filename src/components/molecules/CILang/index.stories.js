import { storiesOf } from '@storybook/vue'
import CILang from './index'

storiesOf('Molecule - ci-lang', module)
	.add('default', () => ({
		components: { 'ci-lang': CILang },
		data: () => ({
			current: 'en'
		}),
		template: `
			<ci-lang v-model="current" />
		`
	}))

