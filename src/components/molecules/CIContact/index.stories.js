import { storiesOf } from '@storybook/vue'
import CIContact from './index'

import VueI18n from 'vue-i18n'
import messages from '@/lang'

storiesOf('Molecule - ci-contact', module)
	.add('default - pt', () => ({
		i18n: new VueI18n({ locale: 'pt', messages }),
		components: { 'ci-contact': CIContact },
		template: `
			<ci-contact />
		`
	}))

	.add('default - en', () => ({
		i18n: new VueI18n({ locale: 'en', messages }),
		components: { 'ci-contact': CIContact },
		template: `
			<ci-contact />
		`
	}))

