<?php

// About page custom fields
function about_add_local_field_groups() {
	$page = get_page_by_path('about');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'About',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_about',
		'title' => 'Vĩdeo',
		'fields' => [
			[
				'key'          => 'ci_about_thumbnail',
				'label'				 => 'Thumbnail',
				'name'         => 'video_thumbnail',
				'type'         => 'image',
			],
			[
				'key'          => 'ci_about_video',
				'label'				 => 'URL',
				'name'         => 'video',
				'type'         => 'oembed',
			]
		],
		'position'   => 'normal',
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			]
		],
	]);
}

// Solutions page custom fields
function apps_add_local_field_groups() {
	$page = get_page_by_path('solutions');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Solutions',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_solutions',
		'title' => 'Soluções',
		'fields' => [
			[
				'key'          => 'ci_solutions_items',
				'name'         => 'items',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_solutions_title',
						'label' => 'Tĩtulo',
						'name'  => 'title',
						'type'  => 'text',
					],
					[
						'key'   => 'ci_solutions_icon',
						'label' => 'Ícone',
						'name'  => 'icon',
						'type'  => 'file',
					],
					[
						'key'   => 'ci_solutions_content',
						'label' => 'Descrição',
						'name'  => 'content',
						'type'  => 'textarea',
					],
				],
				'button_label' => 'Adicionar',
				'min'          => '',
				'max'          => '',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Suite page custom fields
function suite_add_local_field_groups() {
	$page = get_page_by_path('suite');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Suite',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_suite',
		'title' => 'Suite',
		'fields' => [
			[
				'key'          => 'ci_suite_items',
				'name'         => 'items',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_suite_title',
						'label' => 'Título',
						'name'  => 'title',
						'type'  => 'text',
					],
					[
						'key'   => 'ci_suite_icon',
						'label' => 'Ícone',
						'name'  => 'icon',
						'type'  => 'text',
					],
					[
						'key'   => 'ci_suite_content',
						'label' => 'Descrição',
						'name'  => 'content',
						'type'  => 'textarea',
					],
				],
				'button_label' => 'Adicionar',
				'min'          => '',
				'max'          => '',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Apps page custom fields
function solutions_add_local_field_groups() {
	$page = get_page_by_path('apps');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Apps',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_apps',
		'title' => 'Aplicativos',
		'fields' => [
			[
				'key'          => 'ci_apps_item',
				'name'         => 'items',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_apps_first_brand',
						'label' => 'Primeira Marca',
						'name'  => 'first_brand',
						'type'  => 'file',
					],
					[
						'key'   => 'ci_apps_second_brand',
						'label' => 'Segunda Marca',
						'name'  => 'second_brand',
						'type'  => 'file',
					],
					[
						'key'   => 'ci_apps_content',
						'label' => 'Descrição',
						'name'  => 'content',
						'type'  => 'textarea',
					],
				],
				'button_label' => 'Adicionar',
				'min'          => '',
				'max'          => '',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Squad page custom fields
function squad_add_local_field_groups() {
	$page = get_page_by_path('squad');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Squad',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_squad',
		'title' => 'Canais',
		'fields' => [
			[
				'key'          => 'ci_squad_feature',
				'name'         => 'features',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_item_feature',
						'label' => 'Categoria',
						'name'  => 'category',
						'type'  => 'text',
					],
					[
						'key'          => 'ci_squad_item',
						'name'         => 'items',
						'type'         => 'repeater',
						'sub_fields'   => [
							[
								'key'   => 'ci_item_image',
								'label' => 'Imagem',
								'name'  => 'image',
								'type'  => 'image',
							],
							[
								'key'   => 'ci_item_label',
								'label' => 'Título',
								'name'  => 'label',
								'type'  => 'text',
							],
						],
						'button_label' => 'Adicionar',
						'min'          => '',
						'max'          => '',
					],
				],
				'button_label' => 'Adicionar',
				'min'          => '',
				'max'          => '',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Contact page custom fields
function contact_add_local_field_groups() {
	$page = get_page_by_path('contact');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Contact',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_contact',
		'title' => 'Contato',
		'fields' => [
			[
				'key'          => 'ci_contact_email',
				'name'         => 'email',
				'label'        => 'Email',
				'instructions' => 'Email do recebimento do formulário.',
				'type'         => 'text',
			],
            [
				'key'          => 'ci_contact_success_title',
				'name'         => 'success_title',
				'label'        => 'Título de Sucesso',
				'instructions' => 'Será exibido na página de sucesso ao enviar o formulário.',
				'type'         => 'text',
			],
            [
				'key'          => 'ci_contact_success_content',
				'name'         => 'success_content',
				'label'        => 'Texto de Sucesso',
				'type'         => 'textarea',
			],
			[
				'key'          => 'ci_contact_social',
				'name'         => 'social',
				'label'        => 'Redes socials',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_contact_icon',
						'label' => 'Ícone',
						'name'  => 'icon',
						'type'  => 'text',
					],
					[
						'key'   => 'ci_contact_link',
						'label' => 'Link',
						'name'  => 'link',
						'type'  => 'text',
					],
				],
				'button_label' => 'Adicionar',
				'min'          => '',
				'max'          => '',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Citrus seed program custom fields
function seed_add_local_field_groups() {
	$page = get_page_by_path('seed');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Seed',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create planting fields
	acf_add_local_field_group([
		'key' => 'ci_seed_planting',
		'title' => 'Plantando',
		'menu_order' => 0,
		'fields' => [
			[
				'key'          => 'ci_seed_planting_title',
				'name'         => 'planting_title',
				'label'        => 'Título',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_planting_content',
				'name'         => 'planting_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create sdc fields
	acf_add_local_field_group([
		'key' => 'ci_seed_sdc',
		'title' => 'Seeds Certification',
		'menu_order' => 1,
		'fields' => [
			[
				'key'          => 'ci_seed_sdc_title',
				'name'         => 'sdc_title',
				'label'        => 'Título',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_sdc_subhead',
				'name'         => 'sdc_subhead',
				'label'        => 'Subtítulo',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_sdc_content',
				'name'         => 'sdc_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_seed_sdc_items',
				'name'         => 'sdc_items',
				'label'        => 'Itens',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_seed_sdc_item_brand',
						'label' => 'Marca',
						'name'  => 'brand',
						'type'  => 'image',
					],
					[
						'key'   => 'ci_seed_sdc_item_content',
						'label' => 'Conteúdo',
						'name'  => 'content',
						'type'  => 'wysiwyg',
					],
					[
						'key'          => 'ci_seed_sdc_item_features',
						'name'         => 'features',
						'label'        => 'Lista',
						'type'         => 'repeater',
						'sub_fields'   => [
							[
								'key'   => 'ci_seed_sdc_item_feature_name',
								'label' => 'Nome',
								'name'  => 'name',
								'type'  => 'text',
							],
							[
								'key'          => 'ci_seed_sdc_item_feature_items',
								'name'         => 'items',
								'label'        => 'Lista',
								'type'         => 'repeater',
								'sub_fields'   => [
									[
										'key'   => 'ci_seed_sdc_item_feature_item_name',
										'label' => 'Nome',
										'name'  => 'name',
										'type'  => 'text',
									],
								],
								'button_label' => 'Adicionar',
								'min'          => '',
								'max'          => '',
							],
						],
						'button_label' => 'Adicionar',
						'min'          => '',
						'max'          => '',
					],
				],
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create swc fields
	acf_add_local_field_group([
		'key' => 'ci_seed_swc',
		'title' => 'Sowers Certification',
		'menu_order' => 2,
		'fields' => [
			[
				'key'          => 'ci_seed_swc_title',
				'name'         => 'swc_title',
				'label'        => 'Título',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_swc_subhead',
				'name'         => 'swc_subhead',
				'label'        => 'Subtítulo',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_swc_content',
				'name'         => 'swc_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_seed_swc_items',
				'name'         => 'swc_items',
				'label'        => 'Itens',
				'type'         => 'repeater',
				'sub_fields'   => [
					[
						'key'   => 'ci_seed_swc_item_brand',
						'label' => 'Marca',
						'name'  => 'brand',
						'type'  => 'image',
					],
					[
						'key'   => 'ci_seed_swc_item_content',
						'label' => 'Conteúdo',
						'name'  => 'content',
						'type'  => 'wysiwyg',
					],
					[
						'key'          => 'ci_seed_swc_item_features',
						'name'         => 'features',
						'label'        => 'Lista',
						'type'         => 'repeater',
						'sub_fields'   => [
							[
								'key'   => 'ci_seed_swc_item_feature_name',
								'label' => 'Nome',
								'name'  => 'name',
								'type'  => 'text',
							],
							[
								'key'          => 'ci_seed_swc_item_feature_items',
								'name'         => 'items',
								'label'        => 'Lista',
								'type'         => 'repeater',
								'sub_fields'   => [
									[
										'key'   => 'ci_seed_swc_item_feature_item_name',
										'label' => 'Nome',
										'name'  => 'name',
										'type'  => 'text',
									],
								],
								'button_label' => 'Adicionar',
								'min'          => '',
								'max'          => '',
							],
						],
						'button_label' => 'Adicionar',
						'min'          => '',
						'max'          => '',
					],
				],
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_seed_transform',
		'title' => 'Transformando limões',
		'menu_order' => 3,
		'fields' => [
			[
				'key'          => 'ci_seed_transform_content',
				'name'         => 'transform_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_about_transform_video_thumbnail',
				'label'				 => 'Thumbnail',
				'name'         => 'transform_video_thumbnail',
				'type'         => 'image',
			],
			[
				'key'          => 'ci_about_transform_video',
				'label'				 => 'URL',
				'name'         => 'transform_video',
				'type'         => 'oembed',
			]
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create fields
	acf_add_local_field_group([
		'key' => 'ci_seed_contact',
		'title' => 'Contato',
		'menu_order' => 4,
		'fields' => [
			[
				'key'          => 'ci_seed_contact_title',
				'name'         => 'contact_title',
				'label'        => 'Título',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_seed_contact_content',
				'name'         => 'contact_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_seed_contact_email',
				'name'         => 'contact_email',
				'label'        => 'Email',
				'instructions' => 'Email do recebimento do formulário.',
				'type'         => 'text',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}

// Lemonade custom fields
function lemonade_add_local_field_groups() {
	$page = get_page_by_path('lemonade');

	// Create page if it doesnt exist
	if (!$page) {
		$page = get_page(
			wp_insert_post([
				'post_title'    => 'Lemonade',
				'post_type'			=> 'page',
				'post_content'  => '',
				'post_status'   => 'publish',
			], true)
		);
	}

	// Create lemonade talk fields
	acf_add_local_field_group([
		'key' => 'ci_lemonade_talk',
		'title' => 'Lemonade Talk',
		'menu_order' => 0,
		'fields' => [
			[
				'key'          => 'ci_lemonade_talk_content',
				'name'         => 'talk_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create lemonade day fields
	acf_add_local_field_group([
		'key' => 'ci_lemonade_day',
		'title' => 'Lemonade Day',
		'menu_order' => 1,
		'fields' => [
			[
				'key'          => 'ci_lemonade_day_content',
				'name'         => 'day_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_lemonade_day_background',
				'name'         => 'day_background',
				'label'        => 'Imagem de fundo',
				'instructions' => '',
				'type'         => 'image',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create lemonade tour fields
	acf_add_local_field_group([
		'key' => 'ci_lemonade_tour',
		'title' => 'Lemonade Tour',
		'menu_order' => 2,
		'fields' => [
			[
				'key'          => 'ci_lemonade_tour_content',
				'name'         => 'tour_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);

	// Create journey tour fields
	acf_add_local_field_group([
		'key' => 'ci_lemonade_journey',
		'title' => 'Lemonade Journey',
		'menu_order' => 3,
		'fields' => [
			[
				'key'          => 'ci_lemonade_journey_title',
				'name'         => 'journey_title',
				'label'        => 'Título',
				'instructions' => '',
				'type'         => 'text',
			],
			[
				'key'          => 'ci_lemonade_journey_content',
				'name'         => 'journey_content',
				'label'        => 'Conteúdo',
				'instructions' => '',
				'type'         => 'wysiwyg',
			],
			[
				'key'          => 'ci_lemonade_journey_email',
				'name'         => 'journey_email',
				'label'        => 'Email',
				'instructions' => 'Email do recebimento do formulário.',
				'type'         => 'text',
			],
		],
		'location' => [
			[
				[
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				],
			],
		],
	]);
}


// /wp/v2/menu - Main menu
function menu_route_callback($response) {
	$menu = wp_get_nav_menu_object('main');

	return wp_get_nav_menu_items($menu->term_id);
}

// /wp/v2/contact - Contact form
function contact_route_callback($response) {
	// Get form data
	$input = $response->get_json_params();

	// Get email from contact page data
	$page = get_page_by_path('contact');
	$email = get_field('email', $page->ID);
	$to = $email ? $email : "contato@digivox.com.br";

	// Validate form
	if (!$input['subject'])
		$error = 'O campo "assunto" é obrigatório.';

	if (!$input['message'])
		$error = 'O campo "mensagem" é obrigatório.';

	if (!$input['phone'])
		$error = 'O campo "telefone" é obrigatório.';

	if (!$input['company'])
		$error = 'O campo "empresa" é obrigatório.';

	if (!$input['email'])
		$error = 'O campo "email" é obrigatório.';

	if (!$input['name'])
		$error = 'O campo "nome" é obrigatório.';

	// Return bad request error
	if ($error)
		return new WP_Error('validation_fail', $error, ['status' => 400]);

	// Send email
	// Return the status
	return wp_mail($to, "Contato - {$input['subject']}",
		// Body
		"<ul>
			<li><b>Nome:</b> {$input['name']}</li>
			<li><b>Email:</b> {$input['email']}</li>
			<li><b>Empresa:</b> {$input['company']}</li>
			<li><b>Telefone:</b> {$input['phone']}</li>
			<li><b>Mensagem:</b> {$input['message']}</li>
		</ul>",
		// Headers
		['Content-Type: text/html; charset=UTF-8']
	);
}

// /wp/v2/contact/seed - Seed contact form
function contact_seed_route_callback($response) {
	// Get form data
	$input = $response->get_json_params();

	// Get email from contact page data
	$page = get_page_by_path('seed');
	$email = get_field('contact_email', $page->ID);
	$to = $email ? $email : "contato@digivox.com.br";

	// Validate form
	if (!$input['subject'])
		$error = 'O campo "assunto" é obrigatório.';

	if (!$input['message'])
		$error = 'O campo "mensagem" é obrigatório.';

	if (!$input['phone'])
		$error = 'O campo "telefone" é obrigatório.';

	if (!$input['company'])
		$error = 'O campo "empresa" é obrigatório.';

	if (!$input['email'])
		$error = 'O campo "email" é obrigatório.';

	if (!$input['name'])
		$error = 'O campo "nome" é obrigatório.';

	// Return bad request error
	if ($error)
		return new WP_Error('validation_fail', $error, ['status' => 400]);

	// Send email
	// Return the status
	return wp_mail($to, "Citrus Seed Contato - {$input['subject']}",
		// Body
		"<ul>
			<li><b>Nome:</b> {$input['name']}</li>
			<li><b>Email:</b> {$input['email']}</li>
			<li><b>Empresa:</b> {$input['company']}</li>
			<li><b>Telefone:</b> {$input['phone']}</li>
			<li><b>Mensagem:</b> {$input['message']}</li>
		</ul>",
		// Headers
		['Content-Type: text/html; charset=UTF-8']
	);
}

// /wp/v2/contact/lemonade - Lemonade contact form
function contact_lemonade_route_callback($response) {
	// Get form data
	$input = $response->get_json_params();

	// Get email from contact page data
	$page = get_page_by_path('lemonade');
	$email = get_field('journey_email', $page->ID);
	$to = $email ? $email : "contato@digivox.com.br";

	// Validate form
	if (!$input['subject'])
		$error = 'O campo "assunto" é obrigatório.';

	if (!$input['message'])
		$error = 'O campo "mensagem" é obrigatório.';

	if (!$input['phone'])
		$error = 'O campo "telefone" é obrigatório.';

	if (!$input['company'])
		$error = 'O campo "empresa" é obrigatório.';

	if (!$input['email'])
		$error = 'O campo "email" é obrigatório.';

	if (!$input['name'])
		$error = 'O campo "nome" é obrigatório.';

	// Return bad request error
	if ($error)
		return new WP_Error('validation_fail', $error, ['status' => 400]);

	// Send email
	// Return the status
	return wp_mail($to, "Citrus Seed Contato - {$input['subject']}",
		// Body
		"<ul>
			<li><b>Nome:</b> {$input['name']}</li>
			<li><b>Email:</b> {$input['email']}</li>
			<li><b>Empresa:</b> {$input['company']}</li>
			<li><b>Telefone:</b> {$input['phone']}</li>
			<li><b>Mensagem:</b> {$input['message']}</li>
		</ul>",
		// Headers
		['Content-Type: text/html; charset=UTF-8']
	);
}

function create_main_menu() {
	// Check if the menu exists
	$menu_name = 'Main';
	$menu_exists = wp_get_nav_menu_object($menu_name);

	// If it doesn't exist, let's create it.
	if(!$menu_exists){
		$menu_id = wp_create_nav_menu($menu_name);

		// Set up default menu items
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Home',
			'menu-item-url' => '#home',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Sobre',
			'menu-item-url' => '#about',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Communication Suite',
			'menu-item-url' => '#suite',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'APPs',
			'menu-item-url' => '#apps',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Squad',
			'menu-item-url' => '#squad',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Contato',
			'menu-item-url' => '#contact',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Lemonade',
			'menu-item-url' => '/lemonade',
			'menu-item-status' => 'publish'
		]);
		wp_update_nav_menu_item($menu_id, 0, [
			'menu-item-title' =>  'Digivox',
			'menu-item-url' => 'https://digivox.com.br',
			'menu-item-status' => 'publish'
		]);
	}
}

// Exposes ACF object to Rest API
function wp_api_encode_acf($data, $post, $context){
	$data['meta'] = array_merge(
		$data['meta'],
		get_fields($post['ID'])
	);

	return $data;
}

// Only set fields if ACF is enabled
if(function_exists('get_fields'))
	add_filter('json_prepare_post', 'wp_api_encode_acf', 10, 3);

// Register hooks
add_action('init', 'create_main_menu');
add_action('acf/init', 'about_add_local_field_groups');
add_action('acf/init', 'solutions_add_local_field_groups');
add_action('acf/init', 'suite_add_local_field_groups');
add_action('acf/init', 'apps_add_local_field_groups');
add_action('acf/init', 'squad_add_local_field_groups');
add_action('acf/init', 'contact_add_local_field_groups');
add_action('acf/init', 'seed_add_local_field_groups');
add_action('acf/init', 'lemonade_add_local_field_groups');

// Custom routes
add_action('rest_api_init', function () {
	// Contact submition
	register_rest_route('wp/v2', '/contact', [
		'methods' => 'POST',
		'callback' => 'contact_route_callback',
	]);
	// Seed contact submition
	register_rest_route('wp/v2', '/contact/seed', [
		'methods' => 'POST',
		'callback' => 'contact_route_callback',
	]);
	// Lemonade contact submition
	register_rest_route('wp/v2', '/contact/lemonade', [
		'methods' => 'POST',
		'callback' => 'contact_route_callback',
	]);
	// Menus route
	register_rest_route('wp/v2', '/menu', [
		'methods' => 'GET',
		'callback' => 'menu_route_callback',
	]);
});
